#ifndef TIM_H_INCLUDED
#define TIM_H_INCLUDED

#include    <stm32f0xx.h>

//tim6
void TIM6_init(void);

//tim3 as pwm
void GPIOC_init_as_pwm(void);
void TIM3_init_as_pwm(void);
void TIM3_set_pwm_val(unsigned int num_ch, unsigned int val);


#endif /* TIM_H_INCLUDED */
