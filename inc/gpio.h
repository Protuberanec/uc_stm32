#ifndef _gpio_h_
#define _gpio_h_

#include    "stm32f0xx.h"

//#define SET_OUT_PORTC

#define GPIOC_SET_OUT(pin, val)     (GPIOC->BSRR = 1 << ((pin) + 16 * (1 - val)))


uint8_t GPIOA_init();
void GPIOA_initialize_interrupt(void);
uint16_t GPIOA_read_data(void);
//uint8_t GPIOC_init(void);
uint8_t GPIOC_init(uint32_t pins, uint8_t direction);
uint16_t GPIOC_SetValue(uint16_t val);
uint16_t GPIOC_ReadVal(void);

#endif
