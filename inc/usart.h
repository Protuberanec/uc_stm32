#ifndef _USART_H_
#define _USART_H_

#include    <stm32f0xx.h>

#define BAUD_RATE   9600


#define GPIO_AFRH_AFR9_USART             ((uint32_t)0x00000010)
#define GPIO_AFRH_AFR10_UASRT            ((uint32_t)0x00000100)



void Usart1_init(void);
    void USART1_gpio_init(void);

void USART1_PutChar(uint8_t _char);
#endif /* _USART_H_ */
