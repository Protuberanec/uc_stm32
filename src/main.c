/*
**
**                           Main.c
**
**
**********************************************************************/
/*
   Last committed:     $Revision: 00 $
   Last changed by:    $Author: $
   Last changed date:  $Date:  $
   ID:                 $Id:  $

**********************************************************************/

#include    "main.h"

void Delay(uint32_t val);
extern unsigned int count_gl;



unsigned short val_blink = 1;

unsigned int count_timer = 0;   //uint32_t
extern unsigned char isPushedBut;

void TIM6_DAC_IRQHandler(void) {
    TIM6->SR &= ~TIM_SR_UIF;
    if (isPushedBut) {
    //if button was pressed
        count_timer++;
        GPIOC_SET_OUT(8, 1);
    }
    else {
        GPIOC_SET_OUT(9, val_blink);
        val_blink ^= 0x01;
        GPIOC_SET_OUT(8, 0);
    }
}

int main(void)
{
    Usart1_init();

    uint8_t temp_char = 0;
    while(1) {
        USART1_PutChar(temp_char);
        temp_char++;

    }
}


void Delay(uint32_t val) {
    for (int i = 0; i < val; i++);   //временная задержка

}





