#include    "usart.h"

GPIO_TypeDef *mGPIOB = GPIOB;
//GPIO_TypeDef *mGPIOA = GPIOA;
USART_TypeDef *mUSART1 = USART1;

void USART1_gpio_init(void) {
    RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
    mGPIOB->MODER |= GPIO_MODER_MODER6_1 | GPIO_MODER_MODER7_1;
//    1 << (6*2 + 1)
//    1 << (7*2 + 1)

//put it to another file
    mGPIOB->MODER |= GPIO_MODER_MODER9_0;
    //pb5 input
}

//void USART1_gpio_init_pa(void) {
//    RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
//    mGPIOA->MODER |= GPIO_MODER_MODER9_1 | GPIO_MODER_MODER10_1;
//    mGPIOA->AFR[1] |= GPIO_AFRH_AFR9_USART | GPIO_AFRH_AFR10_USART;
//}

void Usart1_init(void) {
    USART1_gpio_init();
    RCC->APB2ENR |= RCC_APB2ENR_USART1EN;

    mUSART1->CR1 |= USART_CR1_TE | USART_CR1_RE;
    mUSART1->CR1 |= USART_CR1_RXNEIE;

    mUSART1->CR3 |= USART_CR3_OVRDIS;

    mUSART1->BRR = 0x1388;

    mUSART1->CR1 |= USART_CR1_UE;
}

void USART1_PutChar(uint8_t _char) {
    while(!(mUSART1->ISR & USART_ISR_TXE));
    mUSART1->TDR = _char;
}
