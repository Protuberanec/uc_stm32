#include    "gpio.h"

RCC_TypeDef *mRCC = RCC;
GPIO_TypeDef *mGPIOA = GPIOA;
EXTI_TypeDef *mEXTI = EXTI;

unsigned int count_gl = 0;
unsigned char isPushedBut = 0;
extern unsigned int count_timer;

void EXTI0_1_IRQHandler(void) {
    static uint8_t enter_first = 0;
    static uint32_t cnt;

    if (EXTI->PR & EXTI_PR_PR0) {
        for (int i = 0; i < 5000; i++);
        EXTI->PR |= EXTI_PR_PR0;
        if (enter_first == 0) {
    //        TIM6->CR1 &= ~TIM_CR1_CEN;
            TIM6->ARR = 5000;
            cnt = TIM6->CNT;
    //        TIM6->CR1 |= TIM_CR1_CEN;
            enter_first = 1;
            isPushedBut = 1;
            count_timer = 0;
        }
        else {
            enter_first = 0;
            isPushedBut = 0;
    //        TIM6->CR1 &= ~TIM_CR1_CEN;-
            TIM6->ARR = count_timer * 5000 + cnt;
    //        TIM6->CR1 |= TIM_CR1_CEN;
        }
    }
}

uint8_t GPIOA_init() {
    mRCC->AHBENR = RCC_AHBENR_GPIOAEN;
    mGPIOA->MODER &= ~GPIO_MODER_MODER0;    //pa0 as input
    GPIOA_initialize_interrupt();
}

void GPIOA_initialize_interrupt(void) {
    EXTI->IMR |= EXTI_IMR_MR0;
    EXTI->EMR |= EXTI_EMR_MR0;
    EXTI->RTSR |= EXTI_RTSR_TR0;
    EXTI->FTSR |= EXTI_FTSR_TR0;

    NVIC_SetPriority(EXTI0_1_IRQn, 2);
    NVIC_EnableIRQ(EXTI0_1_IRQn);
}

uint16_t GPIOA_read_data(void) {
    return mGPIOA->IDR;
}



uint8_t GPIOC_init(uint32_t pins, uint8_t direction) {
    RCC->AHBENR |= RCC_AHBENR_GPIOCEN;  //включение тактирования периферии
    if (pins == 0) {
        GPIOC->MODER |= GPIO_MODER_MODER8_0 | GPIO_MODER_MODER9_0;    //настраиваем на выход
        return 0;
    }

    if (direction == 1) {
        GPIOC->MODER |= pins;
        return 0;
    }

    return 1;
}

uint16_t GPIOC_SetValue(uint16_t val) {
    GPIOC->ODR = val;   // ^ - xor
                //установка выхода либо 1 либо 0
    return GPIOC->IDR;
}

uint16_t GPIOC_ReadVal(void) {
    return GPIOC->IDR;
}
