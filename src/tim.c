#include    "tim.h"

TIM_TypeDef *mTIM6 = TIM6;
TIM_TypeDef *mTIM3 = TIM3;
RCC_TypeDef *mRCC1 = RCC;

void TIM6_init(void) {
    RCC->APB1ENR |= RCC_APB1ENR_TIM6EN;

    TIM6->ARR = 5000;
    TIM6->PSC = 1000;
    TIM6->CR1 |= TIM_CR1_ARPE;

    TIM6->DIER |= TIM_DIER_UIE;

    NVIC_SetPriority(TIM6_DAC_IRQn, 3);
    NVIC_EnableIRQ(TIM6_DAC_IRQn);

    TIM6->CR1 |= TIM_CR1_CEN;
}

void GPIOC_init_as_pwm(void) {
    mRCC1->AHBENR |= RCC_AHBENR_GPIOCEN;
    GPIOC->MODER |= GPIO_MODER_MODER8_1 | GPIO_MODER_MODER9_1;
}

void TIM3_init_as_pwm(void) {
    GPIOC_init_as_pwm();
    mRCC1->APB1ENR |= RCC_APB1ENR_TIM3EN;

    mTIM3->CR1 |= TIM_CR1_ARPE;

    mTIM3->CCMR2 |= TIM_CCMR2_OC3M_1 | TIM_CCMR2_OC3M_2;    //pwm mode 1 cnt < ccr1 is 1
    mTIM3->CCMR2 |= TIM_CCMR2_OC4M_1 | TIM_CCMR2_OC4M_2;

    mTIM3->CCER |= TIM_CCER_CC3E | TIM_CCER_CC4E | TIM_CCER_CC4P;

    mTIM3->ARR = 10000;
    mTIM3->PSC = 1;

    mTIM3->CR1 |= TIM_CR1_CEN;
}

void TIM3_set_pwm_val(unsigned int num_ch, unsigned int val) {
    switch (num_ch) {
        case 3 :
            mTIM3->CCR3 = val;
        break;
        case 4 :
            mTIM3->CCR4 = val;
        break;
        default :

        break;
    }
}















